FROM nvidia/cuda:11.7.0-devel-ubuntu22.04 AS builder

RUN apt update
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt install -y \
    git \
    cmake \
    build-essential \
    mesa-common-dev

RUN git clone https://github.com/ethereum-mining/ethminer.git 

WORKDIR /ethminer 

RUN git submodule update --init --recursive
RUN mkdir build

WORKDIR /ethminer/build

RUN cmake ..
RUN cmake --build .
RUN make all
RUN make install

FROM nvidia/cuda:11.7.0-base-ubuntu22.04
WORKDIR /usr/local/bin
COPY --from=builder /usr/local/bin/ ./
ENTRYPOINT ["/usr/local/bin/ethminer"]  



